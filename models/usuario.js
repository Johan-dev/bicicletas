var mongose = require("mongoose");
var Reserva = requie('./reserva');
var Schema = mongose.Schema;

var usuarioSchema = new Schema({
    nombre: String,
});

usuarioSchema.methods.reservar = function(biciId,desde,hasta,cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
};

module.exports = mongose.model('Usuario', usuarioSchema);
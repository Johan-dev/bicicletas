var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");

describe("Testing bicicletas", function () {
  beforeEach(function (done) {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    var db = mongoose.connection;
    db.on("error", console.error.bind(console, "MongoDB connection error: "));
    db.once("open", function () {
      console.log("We are connected to the database");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (error, sucess) {
      if (error) console.log(error);
      mongoose.disconnect(error);
      done();
    });
  });

  describe("Bicicleta.createinstance", () => {
    it("crea una instancia de la bicicleta", () => {
      var bici = Bicicleta.createInstance(1, "dorado", "todoterreno", [
        -34.5,
        -55.4,
      ]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("dorado");
      expect(bici.modelo).toBe("todoterreno");
      expect(bici.ubicacion[0]).toBe(-34.5);
      expect(bici.ubicacion[1]).toBe(-55.4);
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("comienza vacia", (done) => {
      Bicicleta.allBicis(function (error, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta.add", () => {
    it("agregamos una bicicleta", () => {
      var aBici = new Bicicleta({ code: 1, color: "roja", modelo: "urbana" });
      Bicicleta.add(aBici, function (error, newBici) {
        if (error) console.error("This: " + error);
        Bicicleta.allBicis(aBici, function (error, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("debe devolver la bicicleta con ese code", (done) => {
      Bicicleta.allBicis(function (error, bicis) {
        console.log(bicis);
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({ code: 1, color: "roja", modelo: "urbana" });

        Bicicleta.add(aBici, function (error, newBici) {
          if (error) console.log(error);

          var aBici2 = new Bicicleta({
            code: 2,
            color: "verde",
            modelo: "todoterreno",
          });

          Bicicleta.add(aBici2, function (error, newBici) {
            if (error) console.log(error);

            Bicicleta.findByCode(1, function (error, targetBici) {
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });
});

// beforeEach(() => {Bicicleta.allBicis = [];});

// describe('Bicicleta.allBicis', ()=>{
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     })
// });

// describe('Bicicleta.add', ()=>{
//     it('agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(1,'roja','urbana',[-34.6012424, -58.3861497]);
//         Bicicleta.add(a);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);

//     });
// });

// describe('Bicicleta.findById', ()=>{
//     it('debe devolver la bicicleta con ese id', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var aBici = new Bicicleta(1,'roja','urbana');
//         var aBici2 = new Bicicleta(2,'verde','todoterreno');
//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);

//         var targetData = Bicicleta.findById(1);
//         expect(targetData.id).toBe(1);
//     })
// });

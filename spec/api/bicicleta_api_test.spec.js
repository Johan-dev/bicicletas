var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

describe("Bicicleta API", () => {
  describe("GET BICICLETAS /", () => {
    it("Status 200", () => {
      expect(Bicicleta.allBicis.length).toBe(0);
      var a = new Bicicleta(1, "dorada", "urbana", [-34.6012424, -58.3861497]);
      Bicicleta.add(a);
      request.get(
        "http://localhost:3000/api/bicicletas",
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
        }
      );
    });
  });
});

describe("POST BICICLETAS /create", () => {
  it("Status 200", (done) => {
    var headers = {"content-type" : "application/json"};
    var aBici = '{"id" : 10, "color" : "dorada", "modelo" : "urbana","latitud": -34, "longitud":-58}';
    request.post({
        headers: headers,
        uri: 'http://localhost:3000/api/bicicletas/create',
        body: aBici
    },function(error,response,body){
        expect(response.statusCode).toBe(200);
        expect(Bicicleta.findById(10).color).toBe("dorada");
        done();
    });
    

  });
});
